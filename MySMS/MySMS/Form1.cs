﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySMS.Model.Entities;
using MySMS.Model.ModelWork;
using MySMS.Properties;



namespace MySMS
{
    public partial class Form1 : Form
    {
        private const double TimeUnit = 60;

        public Form1()
        {
            InitializeComponent();
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        public ModelSettings InitializeSettings()
        {
            var modelSettings = new ModelSettings();

            modelSettings.Statistics.WorkingTime = Convert.ToDouble(textBox31.Text);

            var mxGeneratePassenger = TimeUnit/Convert.ToDouble(textBox1.Text);
            var dxGeneratePassenger = TimeUnit/Convert.ToDouble(textBox2.Text);

            modelSettings.PassengerSettings.GenerateNextPassengerTime = new TimeSettings(mxGeneratePassenger,dxGeneratePassenger);
            modelSettings.PassengerSettings.ProbabilityOfHavingTicket = Convert.ToDouble(textBox27.Text);
            modelSettings.PassengerSettings.ProbabilityOfGateCrasher = Convert.ToDouble(textBox12.Text);


            for (int i = 0; i < Convert.ToInt32(textBox3.Text); i++)
            {
                modelSettings.TicketOfficesSettings.Add(new SimpleServingMachineSettings(new TimeSettings(Convert.ToDouble(textBox4.Text), Convert.ToDouble(textBox5.Text))));
            }

            for (int i = 0; i < Convert.ToInt32(textBox6.Text); i++)
            {
                modelSettings.TurnstileSettings.Add(new SimpleServingMachineSettings(new TimeSettings(Convert.ToDouble(textBox7.Text), Convert.ToDouble(textBox8.Text))));
            }

            
            modelSettings.TrainSettings.Add(new TrainSettings(Convert.ToDouble(textBox13.Text), Convert.ToDouble(textBox17.Text), Convert.ToInt32(textBox15.Text)));
            modelSettings.TrainSettings.Add(new TrainSettings(Convert.ToDouble(textBox18.Text), Convert.ToDouble(textBox14.Text), Convert.ToInt32(textBox16.Text)));
            modelSettings.TrainSettings.Add(new TrainSettings(Convert.ToDouble(textBox11.Text), Convert.ToDouble(textBox9.Text), Convert.ToInt32(textBox10.Text)));

            return modelSettings;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            var modelSettings = InitializeSettings();
            ModelManager.Instance.Initialize(modelSettings);
            ModelManager.Instance.ModelWholeSystem();

            textBox23.Text = ModelManager.Instance.PassengerExistanceTime().ToString();
        
        }
    
    }
}
