﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleSMS.ModelWork;

namespace SimpleSMS.ModelWork.Entities
{
    class InfoSource
    {
        private Storage storage;
        private double probabilityOfReturn;
        private UniformDistributionSettings uniformDistributionSettings;

        public event EventHandler<ModelingEventArgs> InfoSourceEvent;

        /// <summary>
        /// Create 
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="returnProb"></param>
        /// <param name="settings"></param>
        public InfoSource(Storage storage, double returnProb, UniformDistributionSettings settings)
        {
            this.storage = storage;
            probabilityOfReturn = returnProb;
            uniformDistributionSettings = settings;
        }

        public void OnRequestProcessed(object sender, Request request)
        {
            var probability = RandomGenerator.GetUniform(0, 1);
            if (probability < probabilityOfReturn)
            {
                storage.AddRequest(request);
            }
        }

        public void CreateRequest()
        {
            GenerationNextRequestComeTime();
        }

        public void SendRequest(Request request)
        {
            storage.AddRequest(request);
            CreateRequest();
        }

        private void GenerationNextRequestComeTime()
        {
            var nextTime = RandomGenerator.GetUniform(uniformDistributionSettings.Left, uniformDistributionSettings.Right);
            InfoSourceEvent(this, new ModelingEventArgs(SendRequest, nextTime, new Request()));
        }

    }
}
