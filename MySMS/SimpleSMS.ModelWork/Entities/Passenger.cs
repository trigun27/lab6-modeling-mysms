﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSMS.ModelWork.Entities
{
    class Passenger
    {
        /// <summary>
        /// Параметр показывает, летил ли человек за границу или перелет местный
        /// </summary>
        public bool IternationalFlieght { get; set; }
        public double AppearTime { get; set; }
    }

    public class PassengerSettings
    {
        public TimeSettings GenerateNextPassengerTime { get; set; }
        public double ProbabilityOfHavingTicket { get; set; }
    }
}
