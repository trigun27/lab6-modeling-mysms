﻿using System;

namespace SimpleSMS.ModelWork.Entities
{
    class ServingMachine
    {
        private bool isFree;
        private NormalDistributionSettings normalDistributionSettings;

        public event EventHandler<ModelingEventArgs> EndOfServingRequest;
        public event EventHandler<Request> RequestSeved;

        public ServingMachine(NormalDistributionSettings settings)
        {
            normalDistributionSettings = settings;
            isFree = true;
        }

        public bool IsFree()
        {
            return isFree;
        }

        public void BeginServeRequest(Request request)
        {
            GenerateServingRequestTime(request);
            isFree = false;
        }


        private void GenerateServingRequestTime(Request request)
        {
            var nextTime = RandomGenerator.GetNormal(normalDistributionSettings.Mean,normalDistributionSettings.Deviation);
            nextTime = Math.Abs(nextTime);
            EndOfServingRequest(this,new ModelingEventArgs(FinishServingRequest, nextTime, request));
        }

        public void FinishServingRequest(Request request)
        {
            isFree = true;
            RequestSeved(this, request);
        }

    }
}
