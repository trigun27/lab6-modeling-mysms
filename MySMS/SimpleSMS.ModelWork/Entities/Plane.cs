﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSMS.ModelWork.Entities
{
    class Plane
    {
    }

    public class PlaneSettings
    {
        public PlaneSettings(double arrivingTime, double waitingInWaitingRoom, int numDepartingPassengers)
        {
            ArrivingTime = arrivingTime;
            WaitingInWaitingRoom = waitingInWaitingRoom;
            NumDepartingPassengers = numDepartingPassengers;
        }
        public double ArrivingTime { get; set; }
        public double WaitingInWaitingRoom { get; set; }
        public int NumDepartingPassengers { get; set; }
    }
}
