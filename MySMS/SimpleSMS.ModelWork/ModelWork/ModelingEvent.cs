﻿using System;

namespace SimpleSMS.ModelWork
{
    public class ModelingEvent
    {
        public ModelingEvent(Action callFunction, double nextCallTime)
        {
            CallFunction = callFunction;
            NextCallTime = nextCallTime;
        }
        public double NextCallTime { get; set; }
        public Action CallFunction { get; set; }
    }

}
