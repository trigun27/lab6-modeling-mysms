﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSMS.ModelWork
{
    class StatisticsModule
    {
        private int numOfPassedPassengers;
        private double agreggateTimeOfAllPassengers;
        private double maxModelingTime;

        public StatisticsModule(StatisticsSettings settings)
        {
            maxModelingTime = settings.WorkingTime;
        }

        public bool DoContinueModeling()
        {
            return ModelManager.Instance.CurrentTime < maxModelingTime;
        }

        public void AddDepartedPassenger(double appearTime)
        {
            agreggateTimeOfAllPassengers += (ModelManager.Instance.CurrentTime - appearTime);
            numOfPassedPassengers++;
        }

        public double GetPassengerExistanceTime()
        {
            return agreggateTimeOfAllPassengers / numOfPassedPassengers;
        }
    }
}
