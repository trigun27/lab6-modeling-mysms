﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSMS.ModelWork.Settings
{
    public class TimeSettings
    {
        public TimeSettings() { }

        public TimeSettings(double parameterA, double parameterB)
        {
            ParameterA = parameterA;
            ParameterB = parameterB;
        }
        public double ParameterA { get; set; }
        public double ParameterB { get; set; }
    }
}
