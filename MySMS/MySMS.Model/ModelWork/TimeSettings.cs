﻿namespace MySMS.Model.ModelWork
{
    public class TimeSettings
    {
        public TimeSettings() { }

        public TimeSettings(double parameterA, double parameterB)
        {
            ParameterA = parameterA;
            ParameterB = parameterB;
        }
        public double ParameterA { get; set; }
        public double ParameterB { get; set; }
    }
}
