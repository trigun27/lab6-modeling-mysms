﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySMS.Model.Entities;
using MySMS.Model.Generator;
using ModelInfoCenter;

namespace MySMS.Model.ModelWork
{
    public class ModelManager
    {
        #region Members
        
        private static volatile ModelManager _instance;
        private static object _syncRoot = new Object();

        // Scheme
        private Enter _enter;
        private List<IServingMachine> _ticketOffices;
        private List<IServingMachine> _turnstiles; 
        private List<IServingMachine> _escalators;
        private List<IServingMachine> _platforms;

        // Model Paameters
        private List<ModelingEvent> modelingEvents;
        private StatisticsModule _statistics;
        private double currentTime;

        #endregion

        #region Constructors
        private ModelManager() { }

        public static ModelManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new ModelManager();
                    }
                    //ModelManager.Instance.Initialize();
                }

                return _instance;
            }
        }

        public void Initialize(ModelSettings settings)
        {
            CreateScheme(settings);
            CreateStatistics(settings);
            InitializeParameters();
        }

        private void CreateScheme(ModelSettings settings)
        {
            _enter = new Enter(settings.PassengerSettings, NewPassenger);
            
            _ticketOffices = new List<IServingMachine>();
            foreach (var setting in settings.TicketOfficesSettings)
            {
                _ticketOffices.Add(new SimpleServingMachine(setting,NewPassenger));
            }

            _turnstiles = new List<IServingMachine>();
            foreach (var setting in settings.TurnstileSettings)
            {
                _turnstiles.Add(new SimpleServingMachine(setting,PassedTurnstile));
            }

           /* _escalators = new List<IServingMachine>();
            foreach (var setting in settings.EscalatorSettings)
            {
                _escalators.Add(new Escalator(setting,PassedEscalator));
            }*/

            _platforms = new List<IServingMachine>();
            foreach (var setting in settings.TrainSettings)
            {
                _platforms.Add(new Platform(setting));
                   
            }

        }

        private void CreateStatistics(ModelSettings settings)
        {
            _statistics = new StatisticsModule(settings.Statistics);
        }

        private void InitializeParameters()
        {
            modelingEvents = new List<ModelingEvent>();
            currentTime = 0;
        }

        #endregion

        #region Methods

        public void ModelWholeSystem()
        {
            _enter.GenerateNextPassengerComeTime();
            foreach (var platform in _platforms)
            {
                platform.StartServingMachine();
            }
            while (_statistics.DoContinueModeling())
            {
                ExecuteCurrentEvent();
            }
        }

        public void NewPassenger(Passenger passenger)
        {
            if (passenger.GateCrasher && !passenger.HasTicket)
            {
                ChooseNextQueue(passenger,_platforms);
            }
            if (passenger.HasTicket)
            {
                ChooseNextQueue(passenger,_turnstiles);
            }
            else
            {
                ChooseNextQueue(passenger, _ticketOffices);
            }
        }

        private void ChooseNextQueue(Passenger passenger, List<IServingMachine> servingMachines)
        {
            if (servingMachines != null && servingMachines.Count>0) // аккуратно, возможно выкидывание пассажира из модели
            {
                var optimalMachine = servingMachines.Aggregate((of1, of2) => of1.QueueLength < of2.QueueLength ? of1 : of2); // находит ОА с минимальной очередью
                optimalMachine.PassengerIn(passenger);
            }
        }

        /*private void ArrivedOnPlatform(Passenger obj)
        {
            throw new NotImplementedException();
        }*/

        private void PassedEscalator(Passenger obj)
        {
            var choosePlatform = RandomGenerator.GetUniform(0, _platforms.Count - 1);
            _platforms[choosePlatform].PassengerIn(obj);
        }

        private void PassedTurnstile(Passenger obj)
        {
            //_escalators[0].PassengerIn(obj);
            ChooseNextQueue(obj,_escalators);
        }
       
        private void ExecuteCurrentEvent()
        {
            if (modelingEvents.Count > 0)
            {
                currentTime = modelingEvents[0].NextCallTime;
                modelingEvents[0].CallFunction();
                modelingEvents.RemoveAt(0);
            }
        }

        public void AddModelingEvent(ModelingEvent modelingEvent)
        {
            modelingEvent.NextCallTime += currentTime;
            
            var nearestEventIndex = modelingEvents.FindIndex(ev => modelingEvent.NextCallTime < ev.NextCallTime);
            if (nearestEventIndex != -1)
            {
                modelingEvents.Insert(nearestEventIndex, modelingEvent);
            }
            else
            {
                modelingEvents.Add(modelingEvent);
            }
        }

        public double PassengerExistanceTime()
        {
            return _statistics.GetPassengerExistanceTime();
        }

        public double CurrentTime
        {
            get { return currentTime; }
        }

        public StatisticsModule Statistics
        {
            get { return _statistics; }
        }

        #endregion

        
    }
}
