﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySMS.Model.ModelWork;

namespace MySMS.Model.Entities
{
    public class PassengerSettings
    {
        public TimeSettings GenerateNextPassengerTime { get; set; }
        public double ProbabilityOfHavingTicket { get; set; }
        public double ProbabilityOfGateCrasher { get; set; }
    }
}
