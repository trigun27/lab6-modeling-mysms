﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySMS.Model.Generator;
using MySMS.Model.ModelWork;

namespace MySMS.Model.Entities
{
    class Enter
    {
        private PassengerSettings _passengerSettings;
        private Action<Passenger> _callBackAfterCreation;

        public Enter(PassengerSettings passengerSettings, Action<Passenger> callBackAfterCreation)
        {
            _passengerSettings = passengerSettings;
            _callBackAfterCreation = callBackAfterCreation;
        }

        public void ProcessNewPassenger(Passenger passenger)
        {
            _callBackAfterCreation(passenger);
            GenerateNextPassengerComeTime();
        }

        public void GenerateNextPassengerComeTime()
        {
            double time = RandomGenerator.GetNormal(_passengerSettings.GenerateNextPassengerTime.ParameterA, _passengerSettings.GenerateNextPassengerTime.ParameterB);
            var passenger = GenerateNextPassenger();
            var modelEvent = new ModelingEvent(() => ProcessNewPassenger(passenger), time);
            ModelManager.Instance.AddModelingEvent(modelEvent);
        }

        private Passenger GenerateNextPassenger()
        {
            var passenger = new Passenger();
            passenger.HasTicket = RandomGenerator.GetBool(_passengerSettings.ProbabilityOfHavingTicket);
            passenger.GateCrasher = RandomGenerator.GetBool(_passengerSettings.ProbabilityOfGateCrasher);
            passenger.AppearTime = ModelManager.Instance.CurrentTime;
            return passenger;
        }
    }
}
