﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySMS.Model.ModelWork;

namespace MySMS.Model.Entities
{
    class Platform: IServingMachine
    {
        private Queue<Passenger> _passengersQueue;
        private Train _train;
        //private Action<Passenger> _callBackAfterServing;
        private bool _trainOnPlatform;
        //private double _numPassengersInTrain;

        public Platform(TrainSettings trainSettings)
        {
            _train = new Train(trainSettings);
           // _callBackAfterServing = callBackAfterServing;
            _passengersQueue = new Queue<Passenger>();
            _trainOnPlatform = false;
        }

        public void PassengerIn(Passenger passenger)
        {
            if (_trainOnPlatform && _train.DepartingCapacity > 0)
            {
                ServePassenger(passenger);
            }
            else
            {
                _passengersQueue.Enqueue(passenger);
            }
        }

        private void ServePassenger(Passenger passenger)
        {
            ModelManager.Instance.Statistics.AddDepartedPassenger(passenger.AppearTime);
            _train.PassengerOut();
            // пассажир зашел в поезд - уехал
        }

        public void PassengerOut(Passenger passenger)
        {
            //_callBackAfterServing(passenger);
        }

        public void StartServingMachine()
        {
            GenerateNextTrain();
        }

        public int QueueLength { get { return _passengersQueue.Count; } }

        public void GenerateNextTrain()
        {
            var trainArrival = new ModelingEvent(TrainArrive, _train.TrainSettings.ArrivingTime);
            ModelManager.Instance.AddModelingEvent(trainArrival);
        }

        private void TrainArrive()
        {
            _trainOnPlatform = true;
            _train.GenerateTrain();
            //ServeArriving();
            ServeDeparting();

            var trainDepart = new ModelingEvent(TrainDepart, _train.TrainSettings.StayingOnPlatformTime);
            ModelManager.Instance.AddModelingEvent(trainDepart);
        }

        /*private void ServeArriving()
        {
            for (int i = 0; i < _train.ArrivingPassengers; i++)
            {
                PassengerOut(new Passenger());
            }
        }*/

        private void ServeDeparting()
        {
            while (_train.DepartingCapacity>0 && _passengersQueue.Count>0)
            {
                ServePassenger(_passengersQueue.Dequeue());
            }
        }

        private void TrainDepart()
        {
            _trainOnPlatform = false;
            GenerateNextTrain();
        }
    }
}
