﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MySMS.Model.Entities
{
    interface IServingMachine
    {
        void PassengerIn(Passenger passenger);
        void PassengerOut(Passenger passenger);
        void StartServingMachine();
        int QueueLength { get; }
    }
}
