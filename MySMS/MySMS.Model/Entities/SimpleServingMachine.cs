﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySMS.Model.Generator;
using MySMS.Model.ModelWork;

namespace MySMS.Model.Entities
{
    class SimpleServingMachine: IServingMachine
    {
        private Queue<Passenger> _passengersQueue;
        private bool _servingInProgress;
        private TimeSettings _serviceTimeSettings;
        private Action<Passenger> _callBackAfterServing;

        public SimpleServingMachine(SimpleServingMachineSettings settings, Action<Passenger> callBackAfterServing)
        {
            _serviceTimeSettings = settings.ServiceTimeSettings;
            _callBackAfterServing = callBackAfterServing;
            _servingInProgress = false;
            _passengersQueue = new Queue<Passenger>();
        }

        public void PassengerIn(Passenger passenger)
        {
            if (_servingInProgress)
            {
                _passengersQueue.Enqueue(passenger);
            }
            else
            {
                ServePassenger(passenger);
            }
        }

        public void PassengerOut(Passenger passenger)
        {
            _servingInProgress = false;
            _callBackAfterServing(passenger);
            ServePassenger();
        }

        public void StartServingMachine()
        {
            throw new NotImplementedException();
        }

        public int QueueLength { get { return _passengersQueue.Count; } }

        private void ServePassenger(Passenger passenger)
        {
            var time = GenerateEndServingTime();
            var modelEvent = new ModelingEvent(() => PassengerOut(passenger), time);
            ModelManager.Instance.AddModelingEvent(modelEvent);
            _servingInProgress = true;
        }

        private void ServePassenger()
        {
            if (_passengersQueue.Count > 0)
            {
                ServePassenger(_passengersQueue.Dequeue());
            }
        }

        private double GenerateEndServingTime()
        {
            return RandomGenerator.GetUniform(_serviceTimeSettings.ParameterA, _serviceTimeSettings.ParameterB);
        }
    }
}
