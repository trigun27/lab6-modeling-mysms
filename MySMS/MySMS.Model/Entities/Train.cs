﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySMS.Model.Entities
{
    class Train
    {
        public Train(TrainSettings trainSettings)
        {
            TrainSettings = trainSettings;
        }
        public TrainSettings TrainSettings { get; set; }

        public int DepartingCapacity { get; set; }

        public void GenerateTrain()
        {
            DepartingCapacity = TrainSettings.NumDepartingPassengers;
        }

        public void PassengerOut()
        {
            DepartingCapacity--;
        }
    }
}
