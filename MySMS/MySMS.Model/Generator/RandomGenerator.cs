﻿using System;

namespace MySMS.Model.Generator
{
    class RandomGenerator
    {
        private static MT19937 RandomMT = new MT19937();
        public static double GetUniform(double a, double b)
        {
            return a + RandomMT.genrand_real1() * (b - a);
        }

        public static int GetUniform(int a, int b)
        {
            return RandomMT.RandomRange(a, b); // проверить работу
        }

        public static double GetNormal(double mean, double standardDeviation)
        {
            return mean + standardDeviation * GetStandardNormal();
        }

        public static bool GetBool(double probabilityOfTrue)
        {
            return GetUniform(0, 1) < probabilityOfTrue;
        }

        // Get normal (Gaussian) random sample with mean 0 and standard deviation 1
        private static double GetStandardNormal()
        {
            // Use Box-Muller algorithm https://ru.wikipedia.org/wiki/%D0%9F%D1%80%D0%B5%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5_%D0%91%D0%BE%D0%BA%D1%81%D0%B0_%E2%80%94_%D0%9C%D1%8E%D0%BB%D0%BB%D0%B5%D1%80%D0%B0 
            double u1 = RandomMT.genrand_real1();
            double u2 = RandomMT.genrand_real1();
            double r = Math.Sqrt(-2.0 * Math.Log(u1));
            double theta = 2.0 * Math.PI * u2;
            return r * Math.Sin(theta);
        }
    }
}
