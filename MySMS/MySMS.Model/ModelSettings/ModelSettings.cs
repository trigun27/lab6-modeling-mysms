﻿using System.Collections;
using System.Collections.Generic;
using MySMS.Model.Entities;

namespace MySMS.Model.ModelWork
{
    public class ModelSettings
    {
        public ModelSettings()
        {
            Statistics = new StatisticsSettings();
            PassengerSettings = new PassengerSettings();
            TicketOfficesSettings = new List<SimpleServingMachineSettings>();
            TurnstileSettings = new List<SimpleServingMachineSettings>();
            TrainSettings = new List<TrainSettings>();
        }
        
        public StatisticsSettings Statistics { get; set; }
        public PassengerSettings PassengerSettings { get; set; }
        public List<SimpleServingMachineSettings> TicketOfficesSettings { get; set; }
        public List<SimpleServingMachineSettings> TurnstileSettings { get; set; }
        public List<TrainSettings> TrainSettings { get; set; }
    }

    public class StatisticsSettings
    {
        public double WorkingTime { get; set; }
    }
}
