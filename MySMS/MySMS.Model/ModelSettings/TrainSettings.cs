﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MySMS.Model.Entities
{
    public class TrainSettings
    {
        public TrainSettings(double arrivingTime, double stayingOnPlatformTime, int numDepartingPassengers)
        {
            ArrivingTime = arrivingTime;
            StayingOnPlatformTime = stayingOnPlatformTime;
            NumDepartingPassengers = numDepartingPassengers;
        }
        public double ArrivingTime { get; set; }
        public double StayingOnPlatformTime { get; set; }
        public int NumDepartingPassengers { get; set; }
    }
}
